FROM openjdk:8-alpine
WORKDIR /tmp
COPY target/review-0.0.1-SNAPSHOT.jar /tmp/review-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "-Xms128m", "-Xmx256m", "/tmp/review-0.0.1-SNAPSHOT.jar"]
EXPOSE 8081
