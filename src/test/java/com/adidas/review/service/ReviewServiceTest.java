package com.adidas.review.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import com.adidas.common.dto.review.ReviewResponseDTO;
import com.adidas.common.dto.review.ReviewScoreDTO;
import com.adidas.common.exception.BaseException;
import com.adidas.review.entity.ReviewScore;
import com.adidas.review.mapper.ReviewMapper;
import com.adidas.review.repository.ReviewScoreRepository;
import com.adidas.review.service.client.ProductApiClient;
import com.adidas.review.service.impl.ReviewServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class ReviewServiceTest {
	
	@InjectMocks
	private ReviewServiceImpl reviewService;
	
	@Mock
	private ReviewScoreRepository reviewScoreRepo;
	
	@Mock
	private ReviewMapper reviewMapper;
	
	@Mock
	private ProductApiClient productApi;
	
	@Before
	public void setup() {
		UserDetails user = new User("1","", new ArrayList<>());
		SecurityContext context = SecurityContextHolder.createEmptyContext();
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
        		user, null, null);
        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);
	}
	
	@Test
	public void testGetReviews() throws Exception {
		
		ReviewScore reviewScore = mock(ReviewScore.class);
		ReviewScoreDTO reviewScoreDTO = mock(ReviewScoreDTO.class);
		when(reviewScoreRepo.findByProductId(Mockito.anyString())).thenReturn(Arrays.asList(reviewScore));
		when(reviewMapper.toDTO(reviewScore)).thenReturn(reviewScoreDTO);
		ReviewResponseDTO res = reviewService.getReviews("ABC123");
		assertEquals(reviewScoreDTO,res.getReviews().get(0));
	}
	
	@Test
	public void testCreateReview() throws Exception {
		
		ReviewScore reviewScore = mock(ReviewScore.class);
		when(productApi.checkIfProductExists(Mockito.anyString())).thenReturn(Boolean.TRUE);
		when(reviewScoreRepo.findByProductIdAndUserId(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.of(reviewScore));
		when(reviewScoreRepo.save(reviewScore)).thenReturn(reviewScore);
		ReviewScoreDTO scoreDTO = new ReviewScoreDTO();
		scoreDTO.setRating(4);
		scoreDTO.setUserId(1L);
		reviewService.createReview("ABC123",scoreDTO);
		verify(reviewScoreRepo,times(1)).save(reviewScore);
		
	}
	
	@Test(expected=BaseException.class)
	public void testCreateReview_Exception() throws Exception {
		
		ReviewScore reviewScore = mock(ReviewScore.class);
		when(productApi.checkIfProductExists(Mockito.anyString())).thenReturn(Boolean.FALSE);
		ReviewScoreDTO scoreDTO = new ReviewScoreDTO();
		scoreDTO.setRating(4);
		scoreDTO.setUserId(1L);
		reviewService.createReview("ABC123",scoreDTO);
		verify(reviewScoreRepo,times(1)).save(reviewScore);
	}
	
	@Test
	public void testDeleteReview() throws Exception {
		
		ReviewScore reviewScore = mock(ReviewScore.class);
		when(productApi.checkIfProductExists(Mockito.anyString())).thenReturn(Boolean.TRUE);
		when(reviewScoreRepo.findByProductIdAndUserId(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.of(reviewScore));
		doNothing().when(reviewScoreRepo).delete(reviewScore);	
		reviewService.deleteReview("ABC123");
		verify(reviewScoreRepo,times(1)).delete(reviewScore);
	}

}
