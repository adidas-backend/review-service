package com.adidas.review.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.adidas.common.dto.review.ReviewResponseDTO;
import com.adidas.common.dto.review.ReviewScoreDTO;
import com.adidas.review.service.ReviewService;
import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)

@WebAppConfiguration
public class ReviewControllerTest {

	@InjectMocks
	private ReviewController reviewController;

	@Mock
	private ReviewService reviewService;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.standaloneSetup(reviewController).build();
	}

	@Test
	public void testGetReviews() throws Exception {

		ReviewResponseDTO res = new ReviewResponseDTO();
		ReviewScoreDTO scoreDTO = new ReviewScoreDTO();
		scoreDTO.setRating(4);
		scoreDTO.setUserId(1L);
		scoreDTO.setProductId("ABC123");
		res.setReviews(Arrays.asList(scoreDTO));
		when(reviewService.getReviews(Mockito.anyString())).thenReturn(res);
		mockMvc.perform(MockMvcRequestBuilders.get("/review/AB123")).andExpect(status().isOk());

	}

	@Test
	public void testCreateReview() throws Exception {
		Gson gson = new Gson();
		ReviewScoreDTO scoreDTO = new ReviewScoreDTO();
		scoreDTO.setRating(4);
		scoreDTO.setUserId(1L);
		scoreDTO.setProductId("ABC123");
		mockMvc.perform(MockMvcRequestBuilders.post("/review/AB123").content(gson.toJson(scoreDTO))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated());

	}
	
	@Test
	public void testDeleteReview() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.delete("/review/AB123")).andExpect(status().isOk());

		
	}

}
