package com.adidas.review.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adidas.review.entity.ReviewScore;

@Repository
public interface ReviewScoreRepository extends JpaRepository<ReviewScore,Long> {

	public List<ReviewScore> findByProductId(String productId);
	
	public Optional<ReviewScore> findByProductIdAndUserId(String productId,Long userId);
}
