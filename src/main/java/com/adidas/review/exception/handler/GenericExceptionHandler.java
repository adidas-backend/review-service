package com.adidas.review.exception.handler;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.adidas.common.dto.error.ErrorResponseDTO;
import com.adidas.common.exception.BaseException;
import com.google.gson.Gson;

import feign.FeignException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class GenericExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponseDTO> getException(Exception ex) {
		ErrorResponseDTO errorResponse = new ErrorResponseDTO();
		errorResponse.setError(ex.getMessage());
		errorResponse.setTimeStampMillis(new Date().getTime());
		log.error("Exception :{}", ex);
		return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);

	}
	
	@ExceptionHandler(BaseException.class)
	public ResponseEntity<ErrorResponseDTO> getBaseException(BaseException ex) {
		ErrorResponseDTO errorResponse = new ErrorResponseDTO();
		errorResponse.setError(ex.getMessage());
		errorResponse.setTimeStampMillis(new Date().getTime());
		errorResponse.setStatus(ex.getCode());
		log.error("BaseException :{}", ex);
		return new ResponseEntity<>(errorResponse, ex.getHttpStatus());

	}


	@ExceptionHandler(FeignException.class)
	public final ResponseEntity<Map<String, Object>> handleRestClientException(FeignException ex) {
		HashMap<String, Object> yourHashMap = new Gson().fromJson(ex.contentUTF8(), HashMap.class);
		log.error("FeignException :{}", ex);
		return new ResponseEntity<>(yourHashMap, HttpStatus.valueOf(ex.status()));
	}

}
