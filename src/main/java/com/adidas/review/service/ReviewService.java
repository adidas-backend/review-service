package com.adidas.review.service;

import com.adidas.common.dto.review.ReviewResponseDTO;
import com.adidas.common.dto.review.ReviewScoreDTO;

public interface ReviewService {
	
	public ReviewResponseDTO getReviews(String productId);
	
	public void createReview(String productId,ReviewScoreDTO reviewScore) ;

	void deleteReview(String productId);

}
