package com.adidas.review.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="productApi",url = "${product.api.url}")
public interface ProductApiClient {
	
	@GetMapping("/product/{productId}/exists")
	public Boolean checkIfProductExists(@PathVariable("productId") String productId);

}
