package com.adidas.review.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.adidas.common.config.SecurityContextHelper;
import com.adidas.common.dto.error.ErrorCodes;
import com.adidas.common.dto.review.ReviewResponseDTO;
import com.adidas.common.dto.review.ReviewScoreDTO;
import com.adidas.common.exception.BaseException;
import com.adidas.review.entity.ReviewScore;
import com.adidas.review.mapper.ReviewMapper;
import com.adidas.review.repository.ReviewScoreRepository;
import com.adidas.review.service.ReviewService;
import com.adidas.review.service.client.ProductApiClient;



@Service
public class ReviewServiceImpl implements ReviewService {
	
	@Autowired
	private ReviewScoreRepository reviewScoreRepo;
	
	@Autowired
	private ReviewMapper reviewMapper;
	
	@Autowired
	private ProductApiClient productApi;
	
	@Value("${product.api.url}")
	String productApiUrl;

	@Override
	public ReviewResponseDTO getReviews(String productId) {
		List<ReviewScore> reviewList = reviewScoreRepo.findByProductId(productId);
		List<ReviewScoreDTO> list = reviewList.stream().map(r->reviewMapper.toDTO(r)).collect(Collectors.toList());
		ReviewResponseDTO reviewResponseDTO = new ReviewResponseDTO();
		reviewResponseDTO.setReviews(list);
		return reviewResponseDTO;
	}

	@Override
	@Transactional
	public void createReview(String productId , ReviewScoreDTO reviewScoreDTO)  {
		Boolean exists = productApi.checkIfProductExists(productId);
		if(!exists) {
			throw new BaseException("Product Not Found", ErrorCodes.NOT_FOUND, HttpStatus.NOT_FOUND);
		}
		Long userId = Long.valueOf(SecurityContextHelper.getUserId());
		reviewScoreDTO.setUserId(userId);
		reviewScoreDTO.setProductId(productId);
		
		Optional<ReviewScore> reviewOptional = reviewScoreRepo.findByProductIdAndUserId(productId, userId);
		ReviewScore review;
		if(reviewOptional.isPresent()) {
			review = reviewOptional.get();
			review.setRating(reviewScoreDTO.getRating());
		}
		else {
			review = reviewMapper.toEntity(reviewScoreDTO);
		}
		reviewScoreRepo.save(review);	
	}
	
	
	@Override
	@Transactional
	public void deleteReview(String productId) {
		
		Boolean exists = productApi.checkIfProductExists(productId);
		if(!exists) {
			throw new BaseException("Product Not Found", ErrorCodes.NOT_FOUND, HttpStatus.NOT_FOUND);
		}
		Long userId = Long.valueOf(SecurityContextHelper.getUserId());
		Optional<ReviewScore> reviewOptional = reviewScoreRepo.findByProductIdAndUserId(productId, userId);
		if(reviewOptional.isPresent()) {
			reviewScoreRepo.delete(reviewOptional.get());
		}
	}

	
	

}
