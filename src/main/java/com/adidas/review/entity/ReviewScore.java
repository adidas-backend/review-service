package com.adidas.review.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="review_score")
@Data
public class ReviewScore {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private int rating;
	
	@Column(nullable = false)
	private String productId;
	
	@Column(nullable = false)
	private Long userId;
}
