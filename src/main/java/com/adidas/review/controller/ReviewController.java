package com.adidas.review.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.adidas.common.dto.review.ReviewResponseDTO;
import com.adidas.common.dto.review.ReviewScoreDTO;
import com.adidas.review.service.ReviewService;

@RestController
@RequestMapping(path = "/review")
public class ReviewController {
	
	@Autowired
	private ReviewService reviewService;
	
	@GetMapping("/{productId}")
	public ResponseEntity<ReviewResponseDTO> getReviews(@PathVariable("productId") String productId) {
		return new ResponseEntity<>(reviewService.getReviews(productId),HttpStatus.OK);
	}
	
	@PostMapping("/{productId}")
	@ResponseStatus(HttpStatus.CREATED)
	public void createReview(@PathVariable("productId") String productId, @Valid @RequestBody ReviewScoreDTO reviewScoreDTO) {
			reviewService.createReview(productId,reviewScoreDTO);
	}
	
	@DeleteMapping("/{productId}")
	@ResponseStatus(HttpStatus.OK)
	public void deleteReview(@PathVariable("productId") String productId) {
		reviewService.deleteReview(productId);
	}

}
