package com.adidas.review.mapper;

import org.mapstruct.Mapper;

import com.adidas.common.dto.review.ReviewScoreDTO;
import com.adidas.review.entity.ReviewScore;


@Mapper(componentModel = "spring")
public interface ReviewMapper {
	
	ReviewScoreDTO toDTO(ReviewScore reviewScore);
	ReviewScore toEntity(ReviewScoreDTO reviewScoreDTO);

}
