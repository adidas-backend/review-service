package com.adidas.review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.adidas.common.config.CustomRequestInterceptor;

@SpringBootApplication
@ComponentScan("com")
@EnableFeignClients
public class ReviewServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReviewServiceApplication.class, args);
	}
	
	@Bean
    public CustomRequestInterceptor basicAuthRequestInterceptor() {
        return new CustomRequestInterceptor();
    }
	
}
