# review-service


**Pre requisites:-**

- Java Developement IDE
- Maven
- Git

CRUD operations for review of a product

**Features :-**

- Caching
- Swagger UI
- Exception Handling
- API Security
- Logging
- Dockerfile
- CI/CD Proposal through Jenkinsfile

**Run Through Docker:-**

1. Building your Docker image

_docker build -t review:0.0.1-SNAPSHOT review/._

2. Running your microservices in Docker containers

_docker run -d --name review -p 8081:8081 review:0.0.1-SNAPSHOT_

**Configure CI/CD Pipeline through Jenkinsfile:-**

Select "Add a Pipeline script from SCM" in Pipeline Section to add the Jenkinsfile that will auto create the stage in the jenkins pipeline.




